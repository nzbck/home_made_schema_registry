# Home Made Schema Regisrty

## Getting started

This project is an illustration of : how a home made shema registry can be implemented.
More explanations in this [Octo's Blog article](https://blog.octo.com/).


##  How to use it

Start `HomeMadeSchemaRegisrtyApplication` :
```shell script
./gradlew run
```

and then launch the folowing command

```shell script
curl http://localhost:8080/schemaRegistry/v1/maSourceDeDonnee/personne
```
This should be the result
```yaml
{
   # Meta data
   "$schema": "https://json-schema.org/draft/2019-09/schema",
   "type": "object",
   
   # Object attributes
   "properties": { 
     "dateNaissance": { "type": "string" },
     "id": { "type": "string" },
     "taille": { "type": "integer" } 
   },
   
   # Extra info
   "required": [ "id" ],
   "title": "Personne",
   "description": "Mise à jour toutes les 2 heures. Source des données maSourceDeDonnée "
}
```

## Source

The library used to generate json schema is [jsonschema-generator](https://github.com/victools/jsonschema-generator). 
Many more listed are [here](https://json-schema.org/implementations.html)