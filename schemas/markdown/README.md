# README

## Top-level Schemas

*   [Personne](./personne.md "Mise à jour toutes les 2 heures") – `-`

## Other Schemas

### Objects



### Arrays



## Version Note

The schemas linked above follow the JSON Schema Spec version: `https://json-schema.org/draft/2019-09/schema`
