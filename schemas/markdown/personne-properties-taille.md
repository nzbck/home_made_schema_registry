# Untitled integer in Personne Schema

```txt
undefined#/properties/taille
```



| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                      |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [personne.schema.json\*](../../out/personne.schema.json "open original schema") |

## taille Type

`integer`
