# Personne Schema

```txt
undefined
```

Mise à jour toutes les 2 heures. Source des données maSourceDeDonnée

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                    |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Allowed               | none                | [personne.schema.json](../../out/personne.schema.json "open original schema") |

## Personne Type

`object` ([Personne](personne.md))

# Personne Properties

| Property                        | Type      | Required | Nullable       | Defined by                                                                             |
| :------------------------------ | :-------- | :------- | :------------- | :------------------------------------------------------------------------------------- |
| [dateNaissance](#datenaissance) | `string`  | Optional | cannot be null | [Personne](personne-properties-datenaissance.md "undefined#/properties/dateNaissance") |
| [id](#id)                       | `string`  | Required | cannot be null | [Personne](personne-properties-id.md "undefined#/properties/id")                       |
| [taille](#taille)               | `integer` | Optional | cannot be null | [Personne](personne-properties-taille.md "undefined#/properties/taille")               |

## dateNaissance



`dateNaissance`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Personne](personne-properties-datenaissance.md "undefined#/properties/dateNaissance")

### dateNaissance Type

`string`

## id



`id`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Personne](personne-properties-id.md "undefined#/properties/id")

### id Type

`string`

## taille



`taille`

*   is optional

*   Type: `integer`

*   cannot be null

*   defined in: [Personne](personne-properties-taille.md "undefined#/properties/taille")

### taille Type

`integer`
