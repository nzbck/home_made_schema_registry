package home_made_schema_regisrty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class HomeMadeSchemaRegisrtyApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeMadeSchemaRegisrtyApplication.class, args);
    }
}