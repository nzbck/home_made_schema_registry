package home_made_schema_regisrty.application.rest.schema_repository;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.victools.jsonschema.generator.SchemaGenerator;
import home_made_schema_regisrty.infrastructure.maSourceDeDonnee.Personne;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(value = "/schemaRegistry/v1")
@Api(value = "Schema Registry", tags = {"schema_registry"})
public class PersonneSchemaEndpoint {

    private final SchemaGenerator schemaGenerator;

    @GetMapping("/maSourceDeDonnee/personne")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ObjectNode> getSchema() {
        return ResponseEntity.ok().body(schemaGenerator.generateSchema(Personne.class));
    }

}