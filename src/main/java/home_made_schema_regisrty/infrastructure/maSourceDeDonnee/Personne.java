package home_made_schema_regisrty.infrastructure.maSourceDeDonnee;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Builder
@Setter
@Schema(title = "Personne", description = "Mise à jour toutes les 2 heures. Source des données maSourceDeDonnée ")
public class Personne {

    @JsonProperty("id")
    @Schema(required = true)
    private UUID id;
    @JsonProperty("date_naissance")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateNaissance;
    @JsonProperty("taille")
    private Integer taille;

}